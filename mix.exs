defmodule RiffRecall.MixProject do
  use Mix.Project

  def project do
    [
      app: :riffrecall,
      version: "0.1.0",
      elixir: "~> 1.6",
      escript: [main_module: RiffRecall],
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ffmpex, "~> 0.5.1"},
      {:raxx, "~> 0.14.14"},
      {:ace, "~> 0.15.11"},
      {:sqlitex, "~> 1.3"},
    ]
  end
end
